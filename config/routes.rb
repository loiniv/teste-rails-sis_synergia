Rails.application.routes.draw do
  mount LoldesignPublisher::Rails::Engine => '/'

  root "home#index"
  
  resources :products, path: 'produtos', except: :destroy do
    collection do 
      resources :available_products, only: :destroy
    end
  end
end
